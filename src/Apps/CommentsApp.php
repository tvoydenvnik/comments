<?php

namespace Tvoydenvnik\Comments\Apps;

use Tvoydenvnik\Comments\Models\Comments;
use Tvoydenvnik\TextParser\TextParser;
use Tvoydenvnik\Comments\Interfaces\ICommentsCacheService;
use Tvoydenvnik\Comments\Interfaces\ICommentsChanelFeed;
use Tvoydenvnik\Comments\Interfaces\ICommentsDBService;

class CommentsApp {


    /**
     * @var $_commentsChanelFeed  ICommentsChanelFeed
     */
    protected $_commentsChanelFeed;

    public function setCommentsChanelFeed(ICommentsChanelFeed $feed){
        $this->_commentsChanelFeed = $feed;
    }


    /**
     * @var $_commentsDBService  ICommentsDBService
     */
    protected $_commentsDBService;
    public function setCommentsDBService(ICommentsDBService $service){
        $this->_commentsDBService = $service;
    }




    /**
     * @var $_commentsCacheService  ICommentsCacheService
     */
    protected $_commentsCacheService;
    public function setCommentsCacheService(ICommentsCacheService $service){
        $this->_commentsCacheService = $service;
    }


    /**
     * @var $_textParser  TextParser
     */
    protected $_textParser;
    public function setTextParser(TextParser $service){
        $this->_textParser = $service;
    }


    /*
     * Только для тестирования
     */
    public function truncate(){
        $this->_commentsChanelFeed->truncate();
        $this->_commentsCacheService->truncate();
    }


    public function addComment(
        //автор комментария (число)
        $nAuthorId,
        //описание канала: entity_type - id типа канала (сообщение, статья и т.п.)   entity_id - id объекта (Ввиде строки!)
        array $arChanel, // array('entity_type'=>1, 'entity_id'=>"23", 'title'=>'Заголовок канала', 'url'=>'Адрес канала')
        //само сообщение
        $sRawMessage,
        //id комментария, на который ответили
        $nParentCommentId = null,
        $arOptions = array()
    ){

        $sEntityId = $arChanel['entity_id'];
        $nEntityType = $arChanel['entity_type'];


        /*
         * -- Подготовим сообщения для сохранение в БД
         */
        $sMessage = $this->_textParser->prepareForSaveInDB($sRawMessage);

        /*
        *  -- Проверим, что сообщение не пустое
        */

        if($sMessage === ''){
            return array("error"=>true, "errorMessage"=>"Комментарий пустой.");
        }

        $timestamp = null;
        if(isset($arOptions['createdAtTimestamp'])){
            $timestamp = $arOptions['createdAtTimestamp'];
        }

        /*
         * -- Сохраним комментарий в БД
         */


        $oComment = $this->_commentsDBService->addComment($nAuthorId,$nEntityType , $sEntityId, $nParentCommentId, $sMessage);
        if($oComment === false){

            return array("error"=>true, "errorMessage"=>"Ошибка сохранения комментария");

        }else{

            /*
             * Добавим комментарий в ленту комментариев
             */

            $this->_commentsChanelFeed->addComment($nEntityType, $sEntityId, $oComment["id"], ($timestamp===null?$this->_getTimestampOfCreatedAt($oComment["created_at"]):$timestamp));

            /*
             * -- Сохраним комментарий в кэше
             */
            if($arOptions['doNotCache']!==true){
                $oComment['message'] = $this->_textParser->toHtml($oComment['message']);
                $this->_commentsCacheService->add( $oComment/* $oComment->commentToArray($this->_textParser)*/);
            }


            return array(
                "id"=>$oComment["id"]
            );
        }

    }

    private function _getTimestampOfCreatedAt($sCreatedAt){
        $timestamp = strtotime ($sCreatedAt);
        if($timestamp === false){
            $timestamp = time();
        }

        return $timestamp;
    }


    /*
    * Послучить последние 3 комментария, для массива каналов
    */
    public function getLastForChannels($arChannels){

        /*
         * -- Получим id комментариев, для каждого канала
         */
        $arResult = $this->_commentsChanelFeed->getLastForChannels($arChannels);


        /*
         * -- Попробуем получить комментарии из кэша
         */

        $arResultFromCache = $this->_commentsCacheService->getComments($arResult['commentsId']);

        $arComments = $arResultFromCache['comments'];

        /*
        * -- Которых нет в кэше, получим из БД
        * Данные полученные из БД поместим в кэш
        * Предварительно TextParser
        */
        $arResultFromDB = array();
        if(is_array($arResultFromCache['notInCache']) && count($arResultFromCache['notInCache'])>0){
            $arResultFromDB = $this->_commentsDBService->getComments($arResultFromCache['notInCache']);

            foreach($arResultFromDB as $key=>$value){

                // -- подготовим сообщение
                $value['message'] = $this->_textParser->toHtml($value['message']);
                //-- добавим их в кэш todo tests
                $this->_commentsCacheService->add($value);

                $arComments[$key] = $value;
            }

        }


        /*
         * -- сформируем author_ids
         */
        $arAuthorIds = array();

        for($i=0, $length=count($arComments); $i<$length;$i++){
            array_push($arAuthorIds, $arComments[$i]['author_id']);
        }

        $arAuthorIds = array_values(array_unique($arAuthorIds));

        return array(
            "channels"=>$arResult,
            "commentsId"=>$arResult['commentsId'],
            "commentsData"=>$arComments,
            "usersId"=>$arAuthorIds,

        );

    }


    /*
     * Есть ли новые комментарии для заданных каналов
     */
    public function hasNewCommentsForChannels($arChannels){

        /*
        * -- Получим id комментариев, для каждого канала
        */
        $arResult = $this->_commentsChanelFeed->hasNewCommentsForChannels($arChannels);

    }


    /*
     * Есть ли новые комментарии для канала
     */
    public function hasNewComments($nEntityType, $sEntityId, $nLastIdComment){

        return $this->_commentsChanelFeed->hasNewComments($nEntityType, $sEntityId, $nLastIdComment);

    }


    /*
     * Получить заданное кол-во комментариев с учетом смещения
     */
    public function get($arChanel, $nCount, $nOffsetFromCommentId){


    }

    /*
     * Получить все комментарии для канала
     */
    public function getAll($arChanel){


    }



    /*
     * Удалить комментарий
     */
    public function delete($nCommentId, $nAuthorId, $isAdmin){

        /*
         *-- удаляем комментарий
         */
        if($this->_commentsDBService->markDeleted($nCommentId, $nAuthorId, $isAdmin) === false){
            return "Описание ошибки";
        }else{

            $this->_commentsCacheService->delete($nCommentId);

            $this->_commentsChanelFeed->delete($nCommentId);
            return true;
        }
    }



}

