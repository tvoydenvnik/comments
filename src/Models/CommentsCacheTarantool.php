<?php

namespace Tvoydenvnik\Comments\Models;


use Tvoydenvnik\Comments\Interfaces\ICommentsCacheService;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

class CommentsCacheTarantool implements ICommentsCacheService{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'comments';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //id
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM'), true);

        //expired - используется для удаления страых сообщений
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'expired', 'tree', false, array(12, 'NUM'), true);
    }

    public function metaData(){

        return array(
            "id"=>array("num"=>1),
            "entity_type"=>array("num"=>2),
            "entity_id"=>array("num"=>3),
            "author_id"=>array("num"=>4),
            "parent_id"=>array("num"=>5),
            "message"=>array("num"=>6),
            "like_pos"=>array("num"=>7),
            "like_neg"=>array("num"=>8),
            "joined"=>array("num"=>9),
            "created_at"=>array("num"=>10),
            "updated_at"=>array("num"=>11),
            "expired"=>array("num"=>12)
        );
    }

    public function prepareDataForSave($arData){
        $arResult = array();
        $arMetaData = $this->metaData();

        foreach($arMetaData as $key=>$value){
            if(array_key_exists($key, $arData)){

                $val = $arData[$key];
                if(is_array($val)){
                    $val = json_encode($val);
                }

                array_push($arResult, $val);

            }else if($key === "expired"){

                $val = time() + 60*60*24*2;//два дня
                array_push($arResult, $val);
            }else{

                array_push($arResult, null);
            }

        }

        return $arResult;
    }


    public function makeEvalForUpdate($arData){
        $arResult = array();
        $arMetaData = $this->metaData();

        foreach($arMetaData as $key=>$value){

            if($key==='id'){
                continue;
            }

            if(array_key_exists($key, $arData)){

                $val = $arData[$key];
                if(is_array($val)){
                    $val = json_encode($val);
                }

                if(is_string($val)){
                    $val = "'" . $val . "'";
                }elseif(is_null($arData[$key])){
                    continue;
                }

                array_push($arResult, "{'=', ".$value["num"].", $val}");

            }

        }

        return implode(",", $arResult);
    }

    public function prepareFromCache($arData){

        $arResult = array();
        $arMetaData = $this->metaData();

        foreach($arMetaData as $key=>$value){
            $index = $value["num"]-1;
            if(array_key_exists($index, $arData)){

                if($key === "expired"){
                    continue;
                }
                $arResult[$key] = $arData[$index];
            }

        }

        return $arResult;

    }

    public function delete($nCommentId){
        $nCommentId = intval($nCommentId);
        $this->_connection->delete(self::$cSPACE_NAME, array($nCommentId) , 'primary');

    }

    public function byId($nCommentId){
        $nCommentId = intval($nCommentId);

        $lResult = $this->_connection->select(self::$cSPACE_NAME, array($nCommentId), 'primary', 1);
        if(is_array($lResult)===false || count($lResult)===0){
            return false;
        }else{
            return $this->prepareFromCache($lResult[0]);
        }
    }

    public function isExist($nPostId){

        return TarantoolHelper::isTupleExist($this->_connection, self::$cSPACE_NAME, $nPostId, 'primary');

    }
    public function add(array $arData){

        if($this->isExist($arData["id"]) === true){
            $this->update($arData);
        }else{
            $this->_connection->insert(self::$cSPACE_NAME, $this->prepareDataForSave($arData));
        }



        //$sEval = "box.space.channels.index.channel:update({ $nEntityType , '$sEntityId' }, {{'+', 4, 1}, {'=', 5, $nTimestamp}, {'=', 6, $nCommentId}})";
        //$this->_connection->evaluate($sEval);

    }

    public function update(array $arData){


        $sEval = "box.space.".self::$cSPACE_NAME.".index.primary:update({ ". intval($arData["id"])." }, {".$this->makeEvalForUpdate($arData)."})";
        $this->_connection->evaluate($sEval);



      //  box.space.posts:update({193012}, {{'=',2,2}})

    }


    public function getComments(array $arPostsId){

        $arResult = array();
        $arNotInCache = array();

        for($i=0, $length=count($arPostsId); $i<$length; $i++){

            $nIdPost = intval($arPostsId[$i]);
            $arPost = $this->byId($nIdPost);

            if($arPost!==false){
                //$arPost['__fromTarantool'] = true;
                $arResult[$nIdPost] = $arPost;
            }else{
                array_push($arNotInCache, $nIdPost);
            }


        }

        return array(
            'comments'=>$arResult,
            'notInCache'=>$arNotInCache
        );


    }


    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}