<?php

namespace Tvoydenvnik\Comments\Models;



use Tvoydenvnik\TarantoolHelper\TarantoolAutoIncrement;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;
use Tvoydenvnik\Comments\Interfaces\ICommentsChanelFeed;

class CommentsChanelFeed implements  ICommentsChanelFeed{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    /**
     * channels_comments
     * Структура
     *      - chanel_id - id канала
     *      - id (число) - id комментария
     *
     *      Индекс:
     *         primary:  tree, parts: {1, 'NUM', 2, 'NUM'}
     * @var string
     */
    private static $cSPACE_CHANNELS_COMMENTS_NAME = 'channels_comments';

    /**
     * channels
     *
     * Структура
     *      - id - (число, автоикремент) - id канала
     *      - entity_type (число) - id типа канала (сообщение, статья и т.п.)
     *      - entity_id (строка) - id объекта.
     *      - num - количество комментариев
     *      - date_update - дата последнего добавления комментария в канал
     *      - lastIdComment - id последнего комментария
     *
     * Индекс:
     *      primary:  tree, parts: {1, 'NUM'}
     *      second:  tree, parts: {2, 'NUM',3, 'STR'}
     * @var string
     */
    private static $cSPACE_CHANNELS_NAME = 'channels';


    public function initSchema($sUserName = 'app'){

        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_CHANNELS_COMMENTS_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_CHANNELS_COMMENTS_NAME , 'primary', 'tree', true, array(1, 'NUM',2, 'NUM'), true);

        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_CHANNELS_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_CHANNELS_NAME , 'primary', 'tree', true, array(1, 'NUM'), true);
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_CHANNELS_NAME , 'channel', 'tree', true, array(2, 'NUM',3, 'STR'), true);

    }

    public function setConnection(\Tarantool $connection){
        $this->_connection = $connection;
    }


    /**
     * Удалить все данные из спейсов
     */
    public function truncate(){
        try{

            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_CHANNELS_NAME);
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_CHANNELS_COMMENTS_NAME);
            TarantoolAutoIncrement::emptyAutoIncrement($this->_connection,self::$cSPACE_CHANNELS_NAME);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_CHANNELS_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_CHANNELS_NAME);
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_CHANNELS_COMMENTS_NAME);
        }

    }


    public function drop(){
        try{

            TarantoolHelper::drop($this->_connection,  self::$cSPACE_CHANNELS_NAME);
            TarantoolHelper::drop($this->_connection,  self::$cSPACE_CHANNELS_COMMENTS_NAME);
            TarantoolAutoIncrement::emptyAutoIncrement($this->_connection,self::$cSPACE_CHANNELS_NAME);
            return true;

        }catch (\Exception $e){
            return false;
        }

    }


    /**
     * Добавить комментарий в ленту
     * @param $nEntityType
     * @param $sEntityId
     * @param $nCommentId
     */
    public function addComment($nEntityType, $sEntityId, $nCommentId, $nTimestamp){


        /*
         * Приведим переменные в соответсвии с типами спейсов
         */
        $nEntityType = intval($nEntityType);
        $sEntityId = strval($sEntityId);
        $nCommentId = intval($nCommentId);
        $nTimestamp = intval($nTimestamp);
        /*
         * Найдем канала, если его нет, то создадим
         */


        try{
            $arChanel = $this->_connection->select(self::$cSPACE_CHANNELS_NAME, array($nEntityType, $sEntityId), "channel");
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_CHANNELS_NAME)===false){
                $this->initSchema();
            }
            $arChanel = $this->_connection->select(self::$cSPACE_CHANNELS_NAME, array($nEntityType, $sEntityId), "channel");
        }

        if(count($arChanel) === 0){
            $nChannelId = TarantoolAutoIncrement::getNewId($this->_connection, self::$cSPACE_CHANNELS_NAME);
            $this->_connection->insert(self::$cSPACE_CHANNELS_NAME,array($nChannelId, $nEntityType, $sEntityId, 1, $nTimestamp, $nCommentId));
        }else{

            $nChannelId = $arChanel[0][0];


            $sEval = "box.space.".self::$cSPACE_CHANNELS_NAME.".index.channel:update({ $nEntityType , '$sEntityId' }, {{'+', 4, 1}, {'=', 5, $nTimestamp}, {'=', 6, $nCommentId}})";
            $this->_connection->evaluate($sEval);

            //большое потребление памяти см.  https://github.com/tarantool/tarantool-php/issues/67
//            $this->_connection->update(self::$cSPACE_CHANNELS_NAME,array($nEntityType, $sEntityId), array(
//                array(
//                    "field" => 3,
//                    "op" => "+",
//                    "arg" => 1
//                ),
//                array(
//                    "field" => 4,
//                    "op" => "=",
//                    "arg" => $nTimestamp
//                ),
//                array(
//                    "field" => 5,
//                    "op" => "=",
//                    "arg" => $nCommentId
//                )
//
//            ), "channel");
        }

        $this->_connection->insert(self::$cSPACE_CHANNELS_COMMENTS_NAME,array($nChannelId, $nCommentId));

    }


    public function getChannel($nEntityType, $sEntityId){
        $nEntityType = intval($nEntityType);
        $sEntityId = strval($sEntityId);
        $arResult=  $this->_connection->select(self::$cSPACE_CHANNELS_NAME, array($nEntityType, $sEntityId), "channel", null, null, TARANTOOL_ITER_EQ);
        if(count($arResult)===0){
            return false;
        }else{
            return array(
                'id'=>$arResult[0][0],
                'entity_type'=>$arResult[0][1],
                'entity_id'=>$arResult[0][2],
                'num_comments'=>$arResult[0][3],
                'date_update'=>$arResult[0][4],
                'last_id_comment'=>$arResult[0][5]
            );
        }

    }

    public function hasNewComments($nEntityType, $sEntityId, $nLastIdComment){

        $arChannel = $this->getChannel($nEntityType, $sEntityId);
        if($arChannel === false){
            return false;
        }else{

            if(intval($arChannel['last_id_comment'])>$nLastIdComment){
                return true;
            }
            return false;

        }
    }


    public function hasNewCommentsForChannels($arChannels){

        for($i=0, $length=count($arChannels); $i<$length;$i++){
            $nEntityType = $arChannels[$i]['entity_type'];
            $sEntityId = $arChannels[$i]['entity_id'];
            $nLastIdComment = intval($arChannels[$i]['last_id_comment']);

            $arChannels[$i]['has_new_comments'] = $this->hasNewComments($nEntityType, $sEntityId, $nLastIdComment);

        }


        return $arChannels;
    }


    public function getLastForChannels($arChannels){


        $arResult = array();
        $arCommentsIds = array();
        for($i=0, $length=count($arChannels); $i<$length;$i++){
            $nEntityType = $arChannels[$i]['entity_type'];
            $sEntityId = $arChannels[$i]['entity_id'];

            $comments = $this->getLast($nEntityType, $sEntityId);

            if($comments!==false){
                array_push($arResult, $comments);

                if(is_array($comments['commentsId'])){
                    $arCommentsIds = array_merge($arCommentsIds, $comments['commentsId']);
                }
            }


        }


        return array(
            'channels'=>$arResult,
            'commentsId'=>$arCommentsIds
        );

    }

    public function getLast($nEntityType, $sEntityId, $nCount =3){

        $nEntityType = intval($nEntityType);
        $sEntityId = strval($sEntityId);

        $channel = $this->getChannel($nEntityType, $sEntityId);
        if($channel === false){
            return false;
        }else{

            $offset = $channel['num_comments'] - $nCount;
            if($offset<0){
                $offset = null;
            }
            $arResult = $this->_connection->select(self::$cSPACE_CHANNELS_COMMENTS_NAME, array($channel['id']), "primary", $nCount, $offset, TARANTOOL_ITER_EQ);

            $arComments = array();
            for($i=0,$length=count($arResult);$i<$length;$i++){
                array_push($arComments, $arResult[$i][1]);
            }

            $channel['commentsId'] = $arComments;

            return $channel;
        }

    }


    public function getAll($nEntityType, $sEntityId){
        $nEntityType = intval($nEntityType);
        $sEntityId = strval($sEntityId);

        $channel = $this->getChannel($nEntityType, $sEntityId);
        if($channel === false){
            return false;
        }else{
            $arResult = $this->_connection->select(self::$cSPACE_CHANNELS_COMMENTS_NAME, array($channel['id']), "primary", null, null, TARANTOOL_ITER_EQ);

            $arComments = array();
            for($i=0,$length=count($arResult);$i<$length;$i++){
                array_push($arComments, $arResult[$i][1]);
            }

            $channel['commentsId'] = $arComments;

            return $channel;
        }


    }

}