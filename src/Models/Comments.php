<?php

namespace Tvoydenvnik\Comments\Models;


use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\MetaData;
use Phalcon\Db\Column;
use Tvoydenvnik\Comments\Interfaces\ICommentsCacheService;
use Tvoydenvnik\Comments\Interfaces\ICommentsDBService;
//use Tvoydenvnik\TextParser\TextParser;

class Comments extends Model implements ICommentsDBService{


    /**
     * @var $id integer
     */
    private $id;
    public function setId($val){
        $this->id = intval($val);
    }

    public function getId(){
        return intval($this->id);
    }


    /**
     * @var $entity_type integer
     */
    public $entity_type;

    /**
     * @var $entity_id string
     */
    public $entity_id;

    /**
     * @var $author_id integer
     */
    public $author_id;

    /**
     * @var $parent_id integer
     */
    private $parent_id;

    public function setParentId($val){
        if($val == 0){
            $this->parent_id = null;
        }else{
            $this->parent_id = $val;
        }
    }

    public function getParentId(){
          return $this->parent_id;
    }

    /**
     * @var $message string
     */
    public $message;


//    /**
//     * @var $photos string
//     */
//    private $photos;
//    public function setPhotos($val){
//        $this->photos = self::_getValueForSetArray($val);
//    }
//
//    public function getPhotos(){
//        return self::_toArray($this->photos);
//    }


    public function commentToArray(Comments $oComment){
        return array(
            "id"=>intval($oComment->id),
            "entity_type"=>intval($oComment->entity_type),
            "entity_id"=>$oComment->entity_id,
            "author_id"=>intval($oComment->author_id),
            "parent_id"=> intval($oComment->getParentId()),
            "message"=>$oComment->message,
            "like_pos"=>intval($oComment->like_pos),
            "like_neg"=>intval($oComment->like_neg),
            "created_at"=>$oComment->created_at,
            "updated_at"=>$oComment->updated_at
        );
    }

    public function getTimestampOfCreatedAt(){
        $timestamp = strtotime ($this->created_at);
        if($timestamp === false){
            $timestamp = time();
        }

        return $timestamp;
    }



//    /**
//     * @var $videos string
//     */
//    public $videos;
//    public function setVideos($val){
//        $this->videos = self::_getValueForSetArray($val);
//    }
//
//    public function getVideos(){
//        return self::_toArray($this->videos);
//    }
//
//
//    /**
//     * @var $attaches string
//     */
//    public $attaches;
//    public function setAttaches($val){
//        $this->attaches = self::_getValueForSetArray($val);
//    }
//
//    public function getAttaches(){
//        return self::_toArray($this->attaches);
//    }


    /**
     * @var $like_pos integer
     */
    public $like_pos;

    /**
     * @var $like_neg integer
     */
    public $like_neg;


    /**
     * @var $created_at integer
     */
    public $created_at;

    /**
     * @var $updated_at integer
     */
    public $updated_at;


    public function addComment($nAuthorId, $nEntityType, $sEntityId, $sMessage, $nParentId/*, array $arPhotos, array $arVideos,array $arAttaches*/){


        $newComment = new Comments();

        $newComment->author_id = $nAuthorId;
        $newComment->entity_type = $nEntityType;
        $newComment->entity_id = $sEntityId;
        $newComment->message = $sMessage;
        $newComment->setParentId($nParentId);
        $bSave = $newComment->save();
        if($bSave === false){
            return false;
        }else{
            return $this->commentToArray($newComment);
        }

    }

    public function updatePost($nCommentId, $nUserId, $sMessage, $bIsAdmin){

        $oComment = Comments::byId($nCommentId);

        if($oComment!==false && ( intval($oComment->author_id) === intval($nUserId) || $bIsAdmin === true) ){

            $oComment->message = $sMessage;
            $bResult = $oComment->update();

            if($bResult!== false){
                return $this->commentToArray($oComment);
            }

        }

        return false;


    }

    /**
     * @param $nCommentId
     * @return Comments
     */
    public static function byId($nCommentId){
        $result = Comments::find($nCommentId);
        if($result->count() === 0){
            return false;
        }else{
            $result->rewind();
            while ($result->valid()) {
                return $result->current();
            }

        }
    }


    public function markDeleted($nCommentId, $nUserId, $bIsAdmin){

        $oComment = Comments::byId($nCommentId);

        if($oComment!==false && ( intval($oComment->author_id) === intval($nUserId) || $bIsAdmin === true) ){


            return $oComment->update(array('deleted'=>1));

        }

        return false;

    }



    public function getSource()
    {
        return "s_comments";
    }


    public function beforeCreate()
    {
        // Set the creation date
        $this->created_at = date('Y-m-d H:i:s');
    }

    public function beforeUpdate()
    {
        // Set the modification date
        $this->updated_at = date('Y-m-d H:i:s');
    }


    public function getComments(array $arCommentsId/*, TextParser $parser = null, ICommentsCacheService $service = null*/){

        $result = Comments::find(
            array(
                'id IN ({ids:array})',
                'bind' => array(
                    'ids' => array_values($arCommentsId)
                )
            )
        );

        $arResult = array();

        $result->rewind();
        while ($result->valid()) {

            /**
             * @var $currentComment Comments
             */
            $currentComment = $result->current();

            $arCommentArray = $this->commentToArray($currentComment);

            $arResult[$arCommentArray['id']] =  $arCommentArray;

            $result->next();
        }

        return $arResult;

    }


//    private static function _toArray($val){
//        try{
//            if($val === null){
//                return array();
//            }else{
//                return json_decode($val, true);
//            }
//        }catch (\Exception $e){
//            return array();
//        }
//    }
//
//    private static function _getValueForSetArray($val){
//        try{
//
//            if(is_array($val)) {
//
//                if (count($val) === 0) {
//                    return null;
//                } else {
//                    return json_encode($val);
//                }
//            }elseif(is_null($val)){
//                return null;
//            }else{
//                return json_encode(json_decode($val, true));
//            }
//
//
//        }catch (\Exception $e){
//            return null;
//        }
//    }

    private function _getCreateTableQuery(){
        return "CREATE TABLE s_comments (
          id int(18) NOT NULL AUTO_INCREMENT COMMENT 'id комментария - соотвествует таблице comments',
          entity_type int(3) NOT NULL COMMENT 'id объекта, который будет комментироваться',
          entity_id varchar(50) NOT NULL COMMENT 'id объекта',
          author_id int(18) NOT NULL COMMENT 'id автора комментария',
          parent_id int(18) DEFAULT NULL COMMENT 'id комментария, на который дан ответ',
          message tinytext DEFAULT NULL COMMENT 'Текст комментария',
          like_pos int(6) DEFAULT NULL COMMENT 'Количество лайков положительных',
          like_neg int(6) DEFAULT NULL COMMENT 'Количество негативных лайков',
          deleted int(1) DEFAULT NULL,
          created_at datetime DEFAULT NULL,
          updated_at datetime DEFAULT NULL,
          UNIQUE INDEX UK_comments_id (id)
        )
        ENGINE = INNODB
        CHARACTER SET utf8
        COLLATE utf8_general_ci;";
    }

    public function metaData()
    {
        return array(
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => array(
                'id', 'entity_type', 'entity_id', 'author_id', 'parent_id', 'message', 'like_pos', 'like_neg', 'created_at', 'updated_at', 'deleted'
            ),

            // Every column part of the primary key
            MetaData::MODELS_PRIMARY_KEY => array(
                'id'
            ),

            // Every column that isn't part of the primary key
            MetaData::MODELS_NON_PRIMARY_KEY => array(
                'entity_type', 'entity_id', 'author_id', 'parent_id', 'message', 'like_pos', 'like_neg', 'created_at', 'updated_at', 'deleted'
            ),

            // Every column that doesn't allows null values
            MetaData::MODELS_NOT_NULL => array(
                'id', 'entity_type', 'entity_id', 'author_id'
            ),

            // Every column and their data types
            MetaData::MODELS_DATA_TYPES => array(
                'id'   => Column::TYPE_INTEGER,
                'entity_type'=> Column::TYPE_INTEGER,
                'entity_id'=> Column::TYPE_VARCHAR,
                'author_id' => Column::TYPE_INTEGER,
                'parent_id' => Column::TYPE_INTEGER,
                'message' => Column::TYPE_TEXT,
                'like_pos' => Column::TYPE_INTEGER,
                'like_neg'=> Column::TYPE_INTEGER,
                'created_at' => Column::TYPE_DATETIME,
                'updated_at'=> Column::TYPE_DATETIME,
                'deleted' => Column::TYPE_INTEGER
            ),

            // The columns that have numeric data types
            MetaData::MODELS_DATA_TYPES_NUMERIC => array(
                'id'   => true,
                'entity_type' => true,
                'author_id' => true,
                'like_pos' => true,
                'like_neg' => true,
                'deleted'=>true
            ),

            // The identity column, use boolean false if the model doesn't have
            // an identity column
            MetaData::MODELS_IDENTITY_COLUMN => 'id',

            // How every column must be bound/casted
            MetaData::MODELS_DATA_TYPES_BIND => array(

                'id'   => Column::BIND_PARAM_INT,
                'entity_type'=> Column::BIND_PARAM_INT,
                'entity_id'=> Column::BIND_PARAM_STR,
                'author_id' => Column::BIND_PARAM_INT,
                'parent_id' => Column::BIND_PARAM_INT,
                'message' => Column::BIND_PARAM_STR,
                'like_pos' => Column::BIND_PARAM_INT,
                'like_neg'=> Column::BIND_PARAM_INT,
                'created_at' => Column::BIND_PARAM_STR,
                'updated_at'=> Column::BIND_PARAM_STR,
                'deleted'=> Column::BIND_PARAM_INT
            ),

            // Fields that must be ignored from INSERT SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_INSERT => array(

            ),

            // Fields that must be ignored from UPDATE SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_UPDATE => array(

            ),

            // Default values for columns
            MetaData::MODELS_DEFAULT_VALUES => array(


                'parent_id' => null,
                'message' => null,
                'like_pos' => null,
                'like_neg'=> null,
                'created_at' => null,
                'updated_at'=> null,
                'deleted'=> null,
            ),

            // Fields that allow empty strings
            MetaData::MODELS_EMPTY_STRING_VALUES => array()
        );
    }

    public function truncate(){
        $all = Comments::find();
        $all->delete();
    }


}