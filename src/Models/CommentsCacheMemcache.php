<?php

namespace Tvoydenvnik\Comments\Models;


use Tvoydenvnik\Comments\Interfaces\ICommentsCacheService;

class CommentsCacheMemcache implements ICommentsCacheService{


    /**
     * @var $_connection \Memcache
     */
    private $_connection;

    public function setConnection(\Memcache $con){
        $this->_connection = $con;


    }

    private function _getKey($nCommentId){
        return 'CommsCache:' .$nCommentId;
    }


    public function update($nCommentId, array $arData){


        $key = $this->_getKey($nCommentId);

        $lResultInCache = $this->_connection->get($key);

        if($lResultInCache === false){

            $lResultInCache = $arData;

        }else{

            try{
                $lResultInCache = json_decode($lResultInCache, true);
            }catch (\Exception $e){
                $lResultInCache = array();
            }

            //перезапишем
            foreach($arData as $key=>$value){
                $lResultInCache[$key]=$value;
            }
        }

        $expire = 60 * 60;//1час
        $this->_connection->set($key, json_encode($lResultInCache), 0, $expire);
    }

    public function get(array $arCommentsId){

        $arKeys = array();
        for($i=0, $length=count($arCommentsId); $i<$length; $i++){
            array_push($arKeys, $this->_getKey($arCommentsId[$i]));
        }

        $arResult = array();

        $arFindComment = array();

        $lResultInCache = $this->_connection->get($arKeys);
        if(is_array($lResultInCache) === false){
            $lResultInCache = array();
        }


        foreach($lResultInCache as $key=>$value){
            try{
                $comment =  json_decode($value, true);
                $comment['__fromMemcache'] = true;
                $id = intval($comment['id']);
                $arResult[$id] =  $comment;
                array_push($arFindComment, $id);
            }catch (\Exception $e){

            }

        }


        return array(
            'comments'=>$arResult,
            'notInCache'=>array_values(array_diff($arCommentsId, $arFindComment))
        );


    }

    /**
     * Удаляет все данные с сервера
     * Только для тестирования
     */
    public function truncate(){
        $this->_connection->flush();
    }
}