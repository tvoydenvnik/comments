<?php


namespace Tvoydenvnik\Comments\Interfaces;


interface ICommentsChanelFeed {

    /*
     * Лента комментариев.
     *
     * Храним в tarantool
     *
     *   Структура:
     *      - id - id комментария (см. ICommentsDBService).
     *      - entity_type - id типа канала (сообщение, статья и т.п.)
     *      - entity_id - (строка) id щбъекта
     *
     *  Назначение:
     *      1. Быстрое получение id последних 3 комментариев
     *      2. Определить: если для канала, новые комментарии и сколько их.
     *      3. Получить заданное кол-во комментариев + смещение. Порядок: id, т.е. по дате создания.
     */
    public function addComment($nEntityType, $sEntityId, $nCommentId, $nTimestamp);


    /*
     * Получить последние 3 комментария, для массива каналов
     */
    public function getLastForChannels($arChannels);

    public function getLast($nEntityType, $sEntityId, $nCount);

    public function getAll($nEntityType, $sEntityId);

    public function hasNewComments($nEntityType, $sEntityId, $nLastIdComment);

    public function hasNewCommentsForChannels($arChannels);

    public function truncate();
//

//
//    public function get();

}