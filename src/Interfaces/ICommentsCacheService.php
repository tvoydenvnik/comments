<?php
namespace Tvoydenvnik\Comments\Interfaces;

interface  ICommentsCacheService {

    public function add(array $arData);

    public function update(array $arData);

    public function delete($nCommentId);

    public function getComments(array $arPostsId);

    public function truncate();


}