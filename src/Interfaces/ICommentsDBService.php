<?php
namespace Tvoydenvnik\Comments\Interfaces;

use Tvoydenvnik\TextParser\TextParser;

interface  ICommentsDBService {


    public function addComment($nAuthorId, $nEntityType, $sEntityId, $sMessage, $nParentId);

    public function updatePost($nPostId, $nUserId, $sMessage, $bIsAdmin);

    public function getComments(array $arCommentsId);

    public function markDeleted($nCommentId, $nUserId, $bIsAdmin);

    public function truncate();

}