<?php


namespace Tvoydenvnik\Сomments\Tests;




use Phalcon\Di;
use Tvoydenvnik\Comments\Models\CommentsChanelFeed;

class CommentsChanelFeedTest extends \PHPUnit_Framework_TestCase
{


    private function getConnection()
    {
        $di =  Di\FactoryDefault::getDefault();

        return $di->get('tarantool');
    }

    public function testPing()
    {
        $_connection = $this->getConnection();
        $this->assertTrue($_connection->ping());
        $_connection->close();
    }

    public function off_testDrop()
    {
        $_connection = $this->getConnection();
        $feed = new CommentsChanelFeed();
        $feed->setConnection($_connection);

        $this->assertTrue($feed->drop());
        $_connection->close();
    }


    public function testAdd()
    {


//        XPROF::init();
//
//        XPROF::start();


        $_connection = $this->getConnection();

        $feed = new CommentsChanelFeed();
        $feed->setConnection($_connection);
        $feed->truncate();


        $channels = 50;
        $commentId = 0;

        $arChannelInfo = array();

        for ($i = 1; $i <= $channels; $i++) {

            $channelType = ($this->isEven($i) ? 2 : 1);
            $channelId = $i;


            $time = 0;
            for ($k = 1; $k <= $channelId; $k++) {
                $commentId = $commentId + 1;
                $time = time();
                $feed->addComment($channelType, $channelId, $commentId, $time);


                $this->assertEquals($feed->hasNewComments($channelType, $channelId, $commentId-1), true, 'hasNewComments');

                $this->assertEquals($feed->hasNewComments($channelType, $channelId, $commentId), false, 'hasNewComments');
            }


            array_push($arChannelInfo, array(
                "entity_type" => $channelType,
                "entity_id" => $channelId,
                "num_comments" => $channelId,
                "date_update" => $time,
                "last_id_comment" => $commentId,
            ));


        }


        //Такого канала не существует
        $this->assertEquals(false, $feed->getChannel(rand(1, 1000), "--" . rand(1, 200)));


        //В данном канале комментариев нет
        $this->assertEquals(false, $feed->getAll(rand(1, 1000), "--" . rand(1, 200)));


        foreach ($arChannelInfo as $key => $val) {

            $channelInBD = $feed->getChannel($val['entity_type'], $val['entity_id']);
            $this->assertNotEquals(false, $channelInBD);


            $this->assertEquals($channelInBD['entity_type'], $val['entity_type'], 'getChanel');
            $this->assertEquals($channelInBD['entity_id'], $val['entity_id'], 'getChanel');
            $this->assertEquals($channelInBD['num_comments'], $val['num_comments'], 'getChanel');
            $this->assertEquals($channelInBD['date_update'], $val['date_update'], 'getChanel');
            $this->assertEquals($channelInBD['last_id_comment'], $val['last_id_comment'], 'getChanel');

            //получим все комментарии
            $arComments = $feed->getAll($channelInBD['entity_type'], $val['entity_id']);
            $this->assertEquals($arComments['commentsId'], $this->_getCommentsHelper($arComments['last_id_comment'], $arComments['num_comments']), "getAll");


            $arLastComments = $feed->getLast($channelInBD['entity_type'], $val['entity_id']);


            $this->assertEquals(array_slice($arComments['commentsId'], -3, 3), $arLastComments ['commentsId'], 'getLast');


        }


        $_connection->close();

        //XPROF::stop();

    }

    private function _getCommentsHelper($lastId, $numComments)
    {

        $arResult = array();

        $firstId = $lastId - $numComments + 1;
        for ($i = 0; $i < $numComments; $i++) {
            array_push($arResult, $firstId + $i);
        }

        return $arResult;

    }

    private function isEven($i)
    {
        return ($i % 2) == 0;
    }


}