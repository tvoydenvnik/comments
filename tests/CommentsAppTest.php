<?php

namespace Tvoydenvnik\Сomments\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Comments\Apps\CommentsApp;
use Tvoydenvnik\Comments\Models\Comments;
use Tvoydenvnik\Comments\Models\CommentsCacheTarantool;
use Tvoydenvnik\Comments\Models\CommentsChanelFeed;
use Tvoydenvnik\TextParser\TextParser;

class CommentsAppTest extends \PHPUnit_Framework_TestCase{



    private function _getCommentsApp(){

        $app = new CommentsApp();
        $app->setTextParser(new TextParser());

        $feed = new CommentsChanelFeed();
        $feed->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $app->setCommentsChanelFeed($feed);

        $app->setCommentsDBService(new Comments());

        $cacheService = new CommentsCacheTarantool();
        $cacheService->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $app->setCommentsCacheService($cacheService);

        return $app;
    }


    public function testAddComment(){

        $app = $this->_getCommentsApp();


        $app->truncate();

        $app->addComment(1,
            array('entity_type'=>1, 'entity_id'=>"23", 'title'=>'Заголовок канала', 'url'=>'Адрес канала'),
            'текст',
            1
        );

        $app->addComment(2,
            array('entity_type'=>1, 'entity_id'=>"23", 'title'=>'Заголовок канала', 'url'=>'Адрес канала'),
            'текст 2',
            1
        );

        $app->addComment(5555,
            array('entity_type'=>1, 'entity_id'=>"23", 'title'=>'Заголовок канала', 'url'=>'Адрес канала'),
            'текст 3',
            33
        );


        $app->addComment(232,
            array('entity_type'=>1, 'entity_id'=>"23", 'title'=>'Заголовок канала', 'url'=>'Адрес канала'),
            'текст 4',
            33
        );

        $app->addComment(232,
            array('entity_type'=>1, 'entity_id'=>"233", 'title'=>'Заголовок канала', 'url'=>'Адрес канала'),
            'текст 4',
            33,
            array(),
            array(),
            array(),
            array('doNotCache'=>true)
        );


        $this->assertEquals(true, true);


    }

    public function testGetLastForChannels(){
        $app = $this->_getCommentsApp();

        $arResult = $app->getLastForChannels(array(
            array('entity_type'=>1, 'entity_id'=>"23"),
            array('entity_type'=>1, 'entity_id'=>"233")
        ));


        $arResult2 = $app->getLastForChannels(array(
            array('entity_type'=>1, 'entity_id'=>"23"),
            array('entity_type'=>1, 'entity_id'=>"233")
        ));

        $r = 1;
    }
}