<?php


require_once "/home/bitrix/www/__php-unit/comments/vendor/autoload.php";


$di = new Phalcon\Di\FactoryDefault();

$di->setDefault($di);

$di->set('db', function () {
    return new Phalcon\Db\Adapter\Pdo\Mysql(
        array(
            "host"     => "192.168.81.134",
            "username" => "remote2342",
            "password" => "remote234",
            "dbname"   => "test"
        )
    );
}, true);

$di->set('tarantool', function(){

    $_connection = new \Tarantool('localhost', 3301);
    $_connection->authenticate('app', 'secret');

    return $_connection;
});


$di->set('memcache', function(){

    $l_MC =  new \Memcache();

    if($l_MC->connect('192.168.81.134', '11211')  === false){
        return false;
    }else{
        return $l_MC;
    }
}, true);








